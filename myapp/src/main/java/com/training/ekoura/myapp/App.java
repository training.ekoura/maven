package com.training.ekoura.myapp;
import java.util.*;
import java.io.*;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws Exception
    {
        System.out.println( "Hello World!" );
        
        
        Properties prop = new Properties(); 
        InputStream input = null;
        
        try {
			input = App.class.getResourceAsStream("/info.properties"); 
			prop.load(input);
		} finally {
			if(input != null)
				input.close();
			// TODO: handle finally clause
		}
        
        System.out.println("Application version : " + prop.getProperty("com.training.ekoura.myapp.version", "?"));
    }
}
